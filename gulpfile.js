const { dest, parallel, series, src, watch } = require('gulp');
const replace = require('gulp-replace');
const sass = require('gulp-sass')(require('node-sass'));
const uglify = require('gulp-uglify');
const fs = require('fs');

function scss() {
    return src('src/scss/*.scss')
        .pipe(sass())
        .pipe(dest('build/css/'));
}

function js() {
    return src('src/js/*.js')
        .pipe(uglify())
        .pipe(dest('build/js/'));
}

function assets() {
    return src('src/assets/*')
        .pipe(dest('public/'));
}

function bundle() {
    return src(['src/index.html'])
        .pipe(replace('<style></style>', function(_) {
            const css = fs.readFileSync('build/css/main.css');
            return `<style>${'\n' + css}</style>`;
        }))
        .pipe(replace('<script></script>', function(_) {
            const js = fs.readFileSync('build/js/app.js');
            return `<script>${js}</script>`;
        }))
        .pipe(dest('public/'));
}

exports.build = series(parallel(scss, js, assets), bundle);

exports.watch = function() {
    watch('src/**/*',
        series(parallel(scss, js, assets), bundle));
};