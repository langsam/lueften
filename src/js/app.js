const DEFAULTS = {
    RHUM_IN: 60,
    TEMP_IN: 17
};

const absoluteHumidity = function(rHum, tempInC) {
    return 100000 * 18.016 / 8314.3 * rHum / 100 * 6.1078
        * 10 ** (7.5 * tempInC / (237.3 + tempInC)) / (tempInC + 273.15);
}

const displayResult = function(ahOut, ahIn) {
    const result = document.querySelector('#result');
    result.style.display = 'inline-block';

    result.innerHTML = `
        <span>Absolute Feutchte draußen: ${ahOut.toFixed(2)} g/m³</span><br>
        <span>Absolute Feuchte innen: ${ahIn.toFixed(2)} g/m³</span><br>
    `;

    if (ahOut > ahIn) {
        result.innerHTML += `
            <span class="highlight">Nicht lüften!</span>
        `
    } else {
        result.innerHTML += `
            <span class="highlight">Lüften!</span>
        `
    }
};

const hideResult = function() {
    const result = document.querySelector('#result');
    result.innerHTML = '';
    result.style.display = 'none';
};    

var init = function() {
    const calculator = document.querySelector('#calculator');
    calculator.style.display = 'block';
    
    const tempOut = document.querySelector('#temp_out');
    const tempOutDisplay = document.querySelector('#temp_out_value');
    tempOutDisplay.textContent = tempOut.value;
    tempOut.oninput = function inputHandler(e) {
        tempOutDisplay.textContent = e.target.value;
        
        hideResult();
    };

    const rHumOut = document.querySelector('#rhum_out');
    const rHumOutDisplay = document.querySelector('#rhum_out_value');
    rHumOutDisplay.textContent = rHumOut.value;
    rHumOut.oninput = function inputHandler(e) {
        rHumOutDisplay.textContent = e.target.value;
        
        hideResult();
    };
    
    const tempIn = document.querySelector('#temp_in');
    tempIn.value = DEFAULTS.TEMP_IN;
    tempIn.oninput = hideResult;
    
    const rHumIn = document.querySelector('#rhum_in');
    rHumIn.value = DEFAULTS.RHUM_IN;
    rHumIn.oninput = hideResult;
    
    const calculate = document.querySelector('#calculate');
    calculate.onclick = function clickhandler(_) {
        const ahOut = absoluteHumidity(+rHumOut.value, +tempOut.value);
        const ahIn = absoluteHumidity(+rHumIn.value, +tempIn.value);
        displayResult(ahOut, ahIn);
    };  
};

window.onload = init;